package com.example.kety.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kety.model.SkinRepo
import kotlinx.coroutines.launch

class SkinViewModel : ViewModel() {
    private val skinRepo by lazy { SkinRepo }

    private val _state = MutableLiveData<List<String>>()
    val state: LiveData<List<String>> get() = _state

    fun getSkins() {
        viewModelScope.launch {
            _state.value = skinRepo.getSkins()
        }
    }
}