package com.example.kety.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kety.model.CategoryRepo
import kotlinx.coroutines.launch

class CategoryViewModel : ViewModel() {
    private val categoryRepo by lazy { CategoryRepo }

    private val _state = MutableLiveData<List<String>>()
    val state: LiveData<List<String>> = _state

    fun getCategories() {
        viewModelScope.launch {
            _state.value = categoryRepo.getCategories()
        }
    }
}