package com.example.kety.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.kety.databinding.ItemSkinBinding
import com.example.kety.view.home.HomeFragmentDirections

class SkinAdapter : RecyclerView.Adapter<SkinAdapter.SkinViewHolder>() {

    private var skins = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SkinViewHolder {
        // create the individual item's UI (the item that will be replicated in the recyclerView)
        val binding = ItemSkinBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SkinViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SkinViewHolder, position: Int) {
        // bind the data to the UI
        val skin = skins[position]
        holder.loadSkin(skin)
        holder.itemView.setOnClickListener { view ->
            val directions = HomeFragmentDirections.actionHomeFragmentToDetailFragment(skin)
            view.findNavController().navigate(directions)
        }
    }

    override fun getItemCount(): Int = skins.size

    fun addSkins(skins: List<String>) {
        this.skins = skins.toMutableList()
    }

    class SkinViewHolder(private val binding: ItemSkinBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadSkin(skin: String) {
            binding.tvSkin.text = skin
        }
    }
}