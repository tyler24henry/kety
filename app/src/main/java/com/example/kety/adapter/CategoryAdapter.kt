package com.example.kety.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.kety.databinding.ItemCategoryBinding
import com.example.kety.view.home.HomeFragmentDirections

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var categories = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val binding = ItemCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categories[position]
        holder.loadCategory(category)
        holder.itemView.setOnClickListener { view ->
            val directions = HomeFragmentDirections.actionHomeFragmentToDetailFragment(category)
            view.findNavController().navigate(directions)
        }
    }

    override fun getItemCount(): Int = categories.size

    fun addCategories(categories: List<String>) {
        this.categories = categories.toMutableList()
        notifyDataSetChanged()
    }

    class CategoryViewHolder(private val binding: ItemCategoryBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadCategory(category: String) {
            binding.tvCategory.text = category
        }
    }
}