package com.example.kety.model.remote

interface SkinService {
    suspend fun getSkins() : List<String>
}