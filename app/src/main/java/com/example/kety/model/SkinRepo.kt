package com.example.kety.model

import com.example.kety.model.remote.SkinService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object SkinRepo {
    private val skinService = object : SkinService {
        override suspend fun getSkins(): List<String> {
            return listOf("Dry", "Oily", "Hairy", "Pale", "Blotchy", "Smelly", "Clean", "Dirty", "Smooth", "Bumpy")
        }
    }

    suspend fun getSkins(): List<String> = withContext(Dispatchers.IO) {
        skinService.getSkins()
    }
}