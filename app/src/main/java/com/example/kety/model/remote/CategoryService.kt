package com.example.kety.model.remote

interface CategoryService {
    suspend fun getCategories() : List<String>
}