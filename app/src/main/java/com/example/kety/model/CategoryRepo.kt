package com.example.kety.model

import com.example.kety.model.remote.CategoryService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object CategoryRepo {
    private val categoryService = object : CategoryService {
        override suspend fun getCategories(): List<String> {
            return listOf("Cars", "Sports", "Teams", "Animals", "Foods", "Colors", "Books", "Movies", "Songs", "Artists")
        }
    }

    suspend fun getCategories(): List<String> = withContext(Dispatchers.IO) {
        categoryService.getCategories()
    }
}