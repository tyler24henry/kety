package com.example.kety.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kety.adapter.CategoryAdapter
import com.example.kety.adapter.SkinAdapter
import com.example.kety.databinding.FragmentHomeBinding
import com.example.kety.viewmodel.CategoryViewModel
import com.example.kety.viewmodel.SkinViewModel

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val categoryViewModel by viewModels<CategoryViewModel>()
    private val skinViewModel by viewModels<SkinViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // get categories
        categoryViewModel.getCategories()
        binding.rvCategory.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        // once we get the categories, state will change, and be observed here
        // then we can update the UI of each item in the recyclerView
        categoryViewModel.state.observe(viewLifecycleOwner) { categories ->
            binding.rvCategory.adapter = CategoryAdapter().apply { addCategories(categories) }
        }

        skinViewModel.getSkins()
        binding.rvSkin.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        skinViewModel.state.observe(viewLifecycleOwner) { skins ->
            binding.rvSkin.adapter = SkinAdapter().apply { addSkins(skins) }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}